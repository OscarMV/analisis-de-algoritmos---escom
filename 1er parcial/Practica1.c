/*
IPN - ESCOM
Enero 2019
Autor: Martinez Vazquez Oscar
*/
/*

Problema planteado: Dado un numero n, generar n numeros aleatorios no repetidos guardados en memoria dinamica.
Restricciones del planteamiento: 
 -> n es un numero entero entre el 1 y el 1000
 -> La salida debe generarse en consola separando los resultados con tabuladores
 -> El programa debe estar escrito en C
*/

#include <stdio.h>    //Bilbioteca estandar para el funcionamiento de un programa en c
#include <stdlib.h>   //Biblioteca estandar para el funcionamiento de un programa en c
#include <stdbool.h>  //Biblioteca para la utilizacion de booleanos
#include<time.h>      //Biblioteca que ayuda a generar la semilla de los numeros aleatorios

// Diccionario de funciones
void print_Array(int*, int);  //Funcion que imprime el arreglo de numeros generados
bool new_value_in_array(int, int*, int); //Funcion booleana que indica si un numero existe en el arreglo

int main(){
	int arr_size;
	int* arr;
	srand(time(0)); 
	/*Diccionario de variables:
		*arr : Apuntador al arreglo que contiene los numeros aleatorios
		arr_size  : Indica el total de numeros aleaotrios a generar
		new_value : Genera un valor que es potencialmente valido para ser agregado en el arreglo
		srad(time(0)); Semilla de los numeros aleatorios
	*/
	while(true){
		printf("Introduce un numero entre 1 y 1000: "); scanf("%d", &arr_size);
		if(arr_size < 1 || arr_size > 1000) printf("Numero invalido\n");
		else{
			arr = (int*)malloc(arr_size*sizeof(int));
			for(int i = 0; i < arr_size; i++){
				while(true){
					int new_value = rand(); // Para acotar el rango de amplitud usar '%'
					if(new_value_in_array(new_value, arr, i)){
						arr[i] = new_value;
						break;
					}
				}
			}
			print_Array(arr, arr_size);
			break;
		}

	}
	return 0;
}

void print_Array(int *arr, int arr_size){
	/*Diccionario de variables:
		*arr : Apuntador al arreglo que contiene los numeros aleatorios
		num  : Indica el numero de elementos en el arreglo actualmente 
	*/
	for(int i = 0; i < arr_size; i++){
		printf("%d\t", arr[i]);
	}
	printf("\n");
}

bool new_value_in_array(int new, int *arr, int arr_size){
	/*Diccionario de variables:
		new  : Indica el nuevo valor que potencialmente puede ser agregado al arreglo
		*arr : Apuntador al arreglo que contiene los numeros aleatorios
		num  : Indica el numero de elementos en el arreglo actualmente 
	*/
	for(int i = 0; i < arr_size; i++){
		if(new == arr[i]) return false;
	}
	return true;
}
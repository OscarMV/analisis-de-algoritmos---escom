/*
IPN - ESCOM
Enero 2019
Autor: Martinez Vazquez Oscar
*/
/*
Problema planteado: Dados dos numeros n y m,generar dos arreglos dinamicos de numeros aleatorios de longitud n y m,
ordenarlos y posteriormente mezclarlos en un nuevo arreglo
Restricciones del planteamiento: 
 -> n y m son numeros enteros entre el 1 y el 1000
 -> La salida debe generarse en consola separando los resultados con tabuladores
 -> El programa debe estar escrito en C
 -> Se deben mostrar los 3 arreglos en pantalla (no necesariamente estar guardado en memoria)
*/

#include <stdio.h>    //Bilbioteca estandar para el funcionamiento de un programa en c
#include <stdlib.h>   //Biblioteca estandar para el funcionamiento de un programa en c
#include <stdbool.h>  //Biblioteca para la utilizacion de booleanos
#include <time.h>     //Biblioteca que ayuda a generar la semilla de los numeros aleatorios

// Diccionario de funciones
void print_Array(int*, int);  //Funcion que imprime el arreglo de numeros generados
void generate_array(int**, int); //Funcion que genera las cadenas aleatorias
void insertion_Sort(int**, int); //Funcion que ordena el arreglo utikizando el metodo de insercion para ordenarlos de
								 //forma ascendente
void merge_Arrays(int*, int, int*, int); //Funcion que mezcla dos arreglos ordenados de forma ascendente

int main(){
	int arr1_size, arr2_size;
	int* arr1;
	int* arr2;
	srand(time(0));
	/*Diccionario de variables:
		*arr1 : Apuntador al primer arreglo de numeros aleatorios
		*arr2 : Apuntador al segundo arreglo de numeros aleatorios
		arr1_size : Indica la longitud del primer arreglo
		arr2_size : Indica la longitud del segundo arreglo
		srad(time(0)); Semilla de los numeros aleatorios
	*/
	printf("Longitud del arreglo 1: "); scanf("%d", &arr1_size);
	printf("Longitud del arreglo 2: "); scanf("%d", &arr2_size);

	arr1 = (int*)malloc(arr1_size*sizeof(int));
	arr2 = (int*)malloc(arr2_size*sizeof(int));
	
	generate_array(&arr1, arr1_size);
	generate_array(&arr2, arr2_size);

	printf("Arreglos\n");
	print_Array(arr1, arr1_size);
	print_Array(arr2, arr2_size);
	
	insertion_Sort(&arr1, arr1_size);
	insertion_Sort(&arr2, arr2_size);
	
	printf("Arreglos ordenados\n");
	print_Array(arr1, arr1_size);
	print_Array(arr2, arr2_size);

	printf("Arreglos combinados\n");
	merge_Arrays(arr1, arr1_size, arr2, arr2_size);
	return 0;
}

void generate_array(int **arr, int arr_size){
	/*Diccionario de variables:
		**arr: Apuntador al arreglo de numeros aleatorios
		arr_size : Indica la longitud del arreglo
	Complejidad algoritmica: O(n)
	*/
	for(int i  = 0; i< arr_size; i++){
		(*arr)[i] = rand() % 5;
	}
}

void print_Array(int *arr, int arr_size){
	/*Diccionario de variables:
		*arr : Apuntador al arreglo que contiene los numeros aleatorios
		arr_size  : Indica el numero de elementos en el arreglo actualmente
	Complejidad algoritmica: O(n)
	*/
	for(int i = 0; i < arr_size; i++){
		printf("%d\t", arr[i]);
	}
	printf("\n");
}

void insertion_Sort(int **arr, int n){ 
	int key, j;
	/*Diccionario de variables:
		**arr: Apuntador al arreglo de numeros aleatorios
		arr_size : Indica la longitud del arreglo
		j: Subindice de insercion
		key: Variable auxiliar
	Complejidad algoritmica: O(n*2)
	*/
	for(int i = 1; i < n; i++){
		key = (*arr)[i];
		j = i-1;
		while (j >= 0 && (*arr)[j] > key) { 
			(*arr)[j+1] = (*arr)[j]; 
			j = j-1; 
		}
		(*arr)[j+1] = key; 
	}
}

void merge_Arrays(int *arr1, int arr1_size, int *arr2, int arr2_size){
	int i, j;
	/*Diccionario de variables:
		*arr1 : Apuntador al arreglo que contiene los numeros aleatorios
		arr1_size  : Indica el numero de elementos en el arreglo actualmente
		*arr2 : Apuntador al arreglo que contiene los numeros aleatorios
		arr1_size  : Indica el numero de elementos en el arreglo actualmente
		i : Subindice del arreglo 1
		j : Subindice del arreglo 2
	Complejidad algoritmica: O(n1 + n2)
	*/
	for(i = 0, j = 0; i < arr1_size && j < arr2_size;){
		if(arr1[i] < arr2[j]){
			printf("%d\t", arr1[i]);
			i++;
		}
		else{
			printf("%d\t", arr2[j]);
			j++;
		}
	}
	for(;i < arr1_size; i++) printf("%d\t", arr1[i]);
	for(;j < arr2_size; j++) printf("%d\t", arr2[j]);
	printf("\n");
}

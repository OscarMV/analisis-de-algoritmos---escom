/*
IPN - ESCOM
Enero 2019
Autor: Martinez Vazquez Oscar
*/
/*
Problema planteado: Implementacion del algoritmo de ordenamiento de mezcla(merge sort) en un arreglo de numeros enteros
de longitud n
Complejidad del algoritmo: O(nlogn)
*/

#include <stdio.h>    //Bilbioteca estandar para el funcionamiento de un programa en c
#include <stdlib.h>   //Biblioteca estandar para el funcionamiento de un programa en c
#include <stdbool.h>  //Biblioteca para la utilizacion de booleanos
#include <time.h>     //Biblioteca que ayuda a generar la semilla de los numeros aleatorios

// Diccionario de funciones
void print_Array(int*, int, int);  //Funcion que imprime un arreglo indicando indices de inicio y final
void generate_array(int **, int); //Funcion que genera las cadenas aleatorias
void merge_Arrays(int**, int, int); //Funcion que mezcla dos arreglos ordenados de forma ascendente
void merge_Sort(int **, int, int); // Funcion que realiza el algoritmo de mezcla, complejidad O(nlogn)
void copy_Subarray(int **, int *, int, int); // Funcion que copia un subarray

int main(){
	/*Diccionario de variables:
		start, end: Estructuras de tipo time que nos sirven para indicar el tiempo que tardo el algoritmo en ejecutarse
		arr_size: Indica la longitud de nuestro arreglo
		arr: Apuntador a nuestro arreglo
		srand(time(0)): Semilla de los nuemros aleatorios
	*/
	time_t start, end;
	int arr_size;
	int* arr;
	srand(time(0));

	printf("Longitud del arreglo: "); scanf("%d", &arr_size);
	generate_array(&arr, arr_size);

	printf("Arreglo original\n");
	//print_Array(arr, 0, arr_size - 1);

	time (&start);
	merge_Sort(&arr, 0, arr_size - 1);
	time (&end);

	printf("Arreglo ordenado\n");
	//print_Array(arr, 0, arr_size - 1);
	
	printf("Tiempo requerido: %lf\n", difftime (end,start));
	return 0;
}

void print_Array(int *arr, int left_index, int right_index){
	/*Diccionario de variables:
		*arr : Apuntador al arreglo que contiene los numeros aleatorios
		left_index : Indica el indice en el que va a iniciar
		right_index: Indica el indice en el que va a finalizar
	*/
	for(int i = left_index; i < right_index + 1; i++) printf("%d\t", arr[i]);
	printf("\n");
}

void generate_array(int **arr, int arr_size){
	/*Diccionario de variables:
		**arr: Apuntador al arreglo de numeros aleatorios
		arr_size : Indica la longitud del arreglo
	*/
	(*arr) = (int*)malloc(arr_size * sizeof(int));
	for(int i  = 0; i< arr_size; i++) (*arr)[i] = rand();
}

void merge_Arrays(int **arr, int left_index, int right_index){
	/*Diccionario de variables:
		*arr : Apuntador al arreglo que contiene los numeros aleatorios
		left_index : Indicador de donde inicia el subarreglo
		right_index: Indicador de donde termina el subarreglo
		middle_index: Indicador de donde se encuentra la mitad del arreglo
		i : Subindice del arreglo temporal 1
		j : Subindice del arreglo temporal 2
		k : Subindice del arreglo original
		*temp1 : Arreglo temporal 1
		*temp2 : Arreglo temporal 2
		temp1_Size : Tamaño del arreglo temporal 1
		temp2_Size : Tamaño del arreglo temporal 2
	*/
	int middle_index = (left_index + right_index) / 2;
	int temp1_Size = middle_index - left_index + 1, temp2_Size = right_index - middle_index;
	int *temp1, *temp2;

	// Genera dos subarreglos para poder realizar los movimientos
	copy_Subarray(&temp1, *(arr), temp1_Size, left_index);
	copy_Subarray(&temp2, *(arr), temp2_Size, 1 + middle_index);

	// Inicia el algoritmo de mezcla de arreglos
	int i, j, k = left_index;
	for(i = 0, j = 0; i < temp1_Size && j < temp2_Size;){
		if(temp1[i] < temp2[j]){
			(*arr)[k] = temp1[i];
			i++;
		}
		else{
			(*arr)[k] = temp2[j];
			j++;
		}
		k++;
	}
	for(;i < temp1_Size; i++, k++) (*arr)[k] = temp1[i];
	for(;j < temp2_Size; j++, k++) (*arr)[k] = temp2[j];
	// Termina el algoritmo de mezcla de arreglos

	// Libera la memoria de los datos
	free(temp1);
	free(temp2);
}

void merge_Sort(int **arr, int left_index, int right_index){
	/*Diccionario de variables:
		**arr : Apuntador al arreglo que contiene los numeros aleatorios
		left_index : Indicador de donde inicia el subarreglo
		right_index: Indicador de donde termina el subarreglo
		middle_index: Indicador de donde se encuentra la mitad del subarreglo
	*/
	if (left_index < right_index){
		int middle_index = (left_index + right_index) / 2;
		merge_Sort(arr, left_index, middle_index);
		merge_Sort(arr, middle_index + 1, right_index);

		merge_Arrays(arr, left_index, right_index);
	}
}

void copy_Subarray(int **new_Array, int *original_Array, int new_Array_Size, int index_Beggining){
	/*Diccionario de variables:
		**new_Array : Apuntador al nuevo subarreglo
		*original_Array: Apuntador al arreglo original
		new_Array_Size: Tamaño del nuevo arreglo
		index_Beggining: Indicador de a partir de donde se va a empezar a copiar el subarreglo
	*/
	(*new_Array) = (int*)malloc(new_Array_Size * sizeof(int));
	for(int i = index_Beggining, j = 0; i < index_Beggining + new_Array_Size; i++, j++)
		(*new_Array)[j] = original_Array[index_Beggining + j];
}
